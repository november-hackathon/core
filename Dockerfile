FROM python
LABEL authors="feofanov"

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
RUN pip install poetry==1.7.1

WORKDIR /app

COPY . .

RUN poetry config virtualenvs.create false \
    && poetry install --only main --no-interaction --no-ansi

COPY ./deploy/cmd/start /start
RUN chmod +x /start
COPY ./deploy/cmd/celery_beat /celery_beat
RUN chmod +x /celery_beat
COPY ./deploy/cmd/celery_worker /celery_worker
RUN chmod +x /celery_worker

EXPOSE 8000

ENTRYPOINT ["./entrypoint.sh"]
CMD ["/start"]
