from django.contrib import admin
from django.contrib.admin import register
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.safestring import mark_safe

from cases.models import Case, Status


@register(Case)
class CaseAdmin(admin.ModelAdmin):
    change_form_template = "admin/cases/custom_change_form.html"
    search_fields = ["name", "description"]
    readonly_fields = ["created_by", "created_at", "updated_at", "tickets"]
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "name",
                    "description",
                    "status",
                    "created_by",
                    "created_at",
                    "updated_at",
                    "tickets",
                ]
            },
        ),
    ]
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "name",
                    "description",
                ),
            },
        ),
    )

    def tickets(self, obj):
        tickets = "<ol>"
        for ticket in obj.tickets.all():
            tickets += '<li><a href="{}">{}</a></li>'.format(
                reverse("admin:tickets_ticket_change", args=(ticket.pk,)),
                f"{ticket.title if ticket.title else ticket.description[:20]}",
            )

        return mark_safe(tickets + "</ol>")

    tickets.short_description = "tickets"

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        return super().get_fieldsets(request, obj)

    def change_view(self, request, object_id, form_url="", extra_context=None):
        if request.method == "POST":
            if request.POST.get("action") == "start_conversation":
                url = (
                    reverse("admin:conversations_conversation_add")
                    + f"?case_id={object_id}"
                )
                return HttpResponseRedirect(url)
        return super().change_view(
            request, object_id, form_url, extra_context=extra_context
        )

    def save_model(self, request, obj, form, change):
        if obj.status == Status.DONE and not obj.resolved_at:
            obj.resolved_at = obj.updated_at
        tickets = obj.tickets.all()
        for ticket in tickets:
            ticket.status = obj.status
        obj.tickets.bulk_update(tickets, ["status"])
        return super().save_model(request, obj, form, change)
