from rest_framework import serializers

from cases.models import Case
from core.api.serializers import StatusSerializer
from tickets.api.serializers import TicketSerializer


class CaseSerializer(serializers.HyperlinkedModelSerializer, StatusSerializer):
    class Meta:
        model = Case
        fields = [
            "url",
            "uid",
            "name",
            "description",
            "status",
            "tickets",
            "created_by",
        ]

    created_by = serializers.CharField(read_only=True, source="created_by.email")
    tickets = TicketSerializer(many=True, read_only=True)
