from django.urls import path, include
from rest_framework import routers

from cases.api.viewsets import CaseViewSet

router = routers.DefaultRouter()
router.register("cases", CaseViewSet)
