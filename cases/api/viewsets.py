from rest_framework import viewsets, permissions

from cases.api.serializers import CaseSerializer
from cases.models import Case


class CaseViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Case.objects.all().order_by("-created_at")
    serializer_class = CaseSerializer
    permission_classes = [permissions.AllowAny]
