from django.db import models
from django_fsm import FSMField

from core.models import Dated, UUIDed, CreatedBy
from gettext import gettext as _


class Status(models.TextChoices):
    NEW = "NEW", _("New")
    IN_PROGRESS = "IN_PROGRESS", _("In Progress")
    DONE = "DONE", _("Done")


class Case(Dated, UUIDed, CreatedBy):
    name = models.CharField(_("Название кейса"), max_length=100)
    description = models.TextField(_("Описание кейса"), null=True, blank=True)
    status = FSMField(_("Текущий статус"), default=Status.NEW, choices=Status.choices)
    resolved_at = models.DateTimeField(_("Дата завершения"), null=True, blank=True)

    def __str__(self):
        return self.name

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        if self.status == Status.DONE and not self.resolved_at:
            self.resolved_at = self.updated_at
        tickets = self.tickets.all()
        for ticket in tickets:
            ticket.status = self.status
        self.tickets.bulk_update(tickets, ["status"])
        super().save(force_insert, force_update, using, update_fields)
