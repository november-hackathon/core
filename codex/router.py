from rest_framework.routers import DefaultRouter

from tickets.api.urls import router as tickets_router
from cases.api.urls import router as cases_router
from files.api.urls import router as files_router
from geo.api.urls import router as geo_router
from companies.api.urls import router as companies_router

router = DefaultRouter()
router.registry.extend(cases_router.registry)
router.registry.extend(tickets_router.registry)
router.registry.extend(files_router.registry)
router.registry.extend(geo_router.registry)
router.registry.extend(companies_router.registry)
