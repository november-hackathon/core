from django.contrib import admin

from companies.models import Company, Regulator, CompanyCategory
from core.admin import SaveCreatedBy


@admin.register(Company)
class CompanyAdmin(SaveCreatedBy, admin.ModelAdmin):
    list_display = [
        "name",
        "category",
        "country",
        "regulator",
    ]
    list_filter = ["category", "country"]
    search_fields = ["name", "category__name", "country__name", "regulator__name"]
    ordering = ["name"]
    readonly_fields = ["created_by", "created_at", "updated_at"]


@admin.register(Regulator)
class RegulatorAdmin(SaveCreatedBy, admin.ModelAdmin):
    list_display = ["name"]
    search_fields = ["name"]
    ordering = ["name"]
    readonly_fields = ["created_by", "created_at", "updated_at"]


@admin.register(CompanyCategory)
class CompanyCategoryAdmin(SaveCreatedBy, admin.ModelAdmin):
    list_display = ["name"]
    search_fields = ["name"]
    ordering = ["name"]
    readonly_fields = ["created_by", "created_at", "updated_at"]
