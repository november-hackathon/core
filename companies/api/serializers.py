from rest_framework import serializers

from companies.models import Company
from geo.api.serializers import CountrySerializer


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields = ["uid", "name", "country"]

    country = CountrySerializer()
