from rest_framework import routers

from companies.api.viewsets import CompanyViewSet

router = routers.DefaultRouter()
router.register("companies", CompanyViewSet)
