from rest_framework import viewsets, permissions

from companies.api.serializers import CompanySerializer
from companies.models import Company


class CompanyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Company.objects.all().order_by("-name")
    serializer_class = CompanySerializer
    permission_classes = [permissions.AllowAny]
    pagination_class = None
    filterset_fields = ["country_id"]
