# Generated by Django 4.2.7 on 2023-11-26 12:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("companies", "0007_alter_company_alternative_emails_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="company",
            name="email",
            field=models.EmailField(
                blank=True, max_length=254, null=True, verbose_name="Основной email"
            ),
        ),
    ]
