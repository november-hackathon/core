from django_jsonform.models.fields import ArrayField
from django.db import models

from core.models import UUIDed, Dated, CreatedBy

from gettext import gettext as _


class Regulator(Dated, UUIDed, CreatedBy):
    name = models.CharField(_("Название регулятора"), max_length=255)
    description = models.TextField(_("Описание регулятора"), null=True, blank=True)
    email = models.EmailField(_("Основной email"), null=True, blank=True)
    alternative_emails = ArrayField(
        verbose_name=_("Дополнительные email"),
        base_field=models.EmailField(),
        null=True,
        blank=True,
    )
    phone = models.CharField(
        _("Основной телефон"), max_length=255, null=True, blank=True
    )
    alternative_phones = ArrayField(
        verbose_name=_("Дополнительные телефоны"),
        base_field=models.CharField(max_length=255),
        null=True,
        blank=True,
    )

    country = models.ForeignKey(
        verbose_name=_("Страна"),
        to="geo.Country",
        on_delete=models.CASCADE,
        related_name="regulators",
        null=True,
    )

    def __str__(self):
        return self.name


class Company(Dated, UUIDed, CreatedBy):
    class Meta:
        verbose_name_plural = "companies"

    name = models.CharField(_("Название компании"), max_length=255)
    description = models.TextField(_("Описание компании"), null=True, blank=True)
    email = models.EmailField(_("Основной email"), null=True, blank=True)
    alternative_emails = ArrayField(
        verbose_name=_("Дополнительные email"),
        base_field=models.EmailField(),
        null=True,
        blank=True,
    )
    phone = models.CharField(
        _("Основной телефон"), max_length=255, null=True, blank=True
    )
    alternative_phones = ArrayField(
        verbose_name=_("Дополнительные телефоны"),
        base_field=models.CharField(max_length=255),
        null=True,
        blank=True,
    )
    category = models.ForeignKey(
        verbose_name=_("Категория компании"),
        to="CompanyCategory",
        on_delete=models.SET_NULL,
        related_name="companies",
        null=True,
    )

    country = models.ForeignKey(
        verbose_name=_("Страна"),
        to="geo.Country",
        on_delete=models.SET_NULL,
        related_name="companies",
        null=True,
    )
    regulator = models.ForeignKey(
        verbose_name=_("Регулятор"),
        to="Regulator",
        on_delete=models.SET_NULL,
        related_name="companies",
        null=True,
        blank=True,
    )

    def __str__(self):
        if self.country:
            return f"{self.name}({self.country.name})"
        return self.name


class CompanyCategory(UUIDed, Dated, CreatedBy):
    class Meta:
        verbose_name_plural = "company categories"

    name = models.CharField(_("Название категории"), max_length=255)
    description = models.TextField(_("Описание категории"), null=True, blank=True)

    def __str__(self):
        return self.name
