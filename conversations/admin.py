from django import forms
from django.contrib import admin
from django.contrib.admin import register
from django.core.exceptions import ValidationError
from django.forms import ModelChoiceField, ChoiceField, Widget
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.safestring import mark_safe

from cases.models import Case
from conversations.email import send_email, send_email_with_attachment
from conversations.models import (
    Conversation,
    Attachment,
    Message,
    EmailTemplate,
    MessageType,
    MessageDirection,
)


class MessageInline(admin.TabularInline):
    model = Message
    extra = 0
    readonly_fields = ["created_by", "created_at", "updated_at"]
    ordering = ["-created_at"]
    fields = ["created_at", "subject", "text", "message_type", "message_direction"]

    def has_change_permission(self, request, obj=None):
        return obj is None

    def has_add_permission(self, request, obj=None):
        return obj is None

    def has_delete_permission(self, request, obj=None):
        return obj is None


# Register your models here.
@register(Conversation)
class ConversationAdmin(admin.ModelAdmin):
    inlines = [MessageInline]
    change_form_template = "admin/conversations/custom_change_form.html"
    search_fields = ["summary", "case__name"]
    list_filter = ["case", "company", "regulator"]
    list_display = ["created_at", "case", "company", "regulator"]
    readonly_fields = ["created_by", "created_at", "updated_at"]
    autocomplete_fields = ["case", "company", "regulator"]
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "summary",
                    "created_by",
                    "created_at",
                    "updated_at",
                    "case",
                    "company",
                    "regulator",
                ]
            },
        ),
    ]

    def change_view(self, request, object_id, form_url="", extra_context=None):
        if request.method == "POST":
            if request.POST.get("action") == "write_email":
                tpl_id = request.POST.get("template")
                if tpl_id:
                    url = (
                        reverse("admin:conversations_message_add")
                        + f"?tpl_id={tpl_id}&conversation_id={object_id}"
                    )
                    return HttpResponseRedirect(url)
        extra_context = extra_context or {}
        extra_context["templates"] = EmailTemplate.objects.all()
        return super().change_view(
            request, object_id, form_url, extra_context=extra_context
        )

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        case_id = request.GET.get("case_id")
        if case_id:
            case = Case.objects.get(pk=case_id)
            form.base_fields["case"].initial = case
        return form


class AttachmentInline(admin.TabularInline):
    model = Attachment
    extra = 1
    fields = ["name", "file"]

    def has_change_permission(self, request, obj=None):
        return obj is None

    def has_add_permission(self, request, obj=None):
        return obj is None

    def has_delete_permission(self, request, obj=None):
        return obj is None


class DynamicModelChoiceField(ChoiceField):
    def to_python(self, value):
        try:
            value = super().to_python(value)
        except ValidationError:
            pass
        return value


class MessageForm(forms.ModelForm):
    to = DynamicModelChoiceField()
    send_mail = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        conversation_id = self.initial.get("conversation_id")
        choices = []
        if conversation_id:
            conversation = Conversation.objects.get(pk=conversation_id)
            if conversation.company:
                choices.append(
                    (
                        conversation.company.email,
                        conversation.company.email,
                    )
                )
                if conversation.company.alternative_emails:
                    for email in conversation.company.alternative_emails:
                        choices.append((email, email))
            if conversation.regulator:
                choices.append(
                    (
                        conversation.regulator.email,
                        conversation.regulator.email,
                    )
                )
                if conversation.regulator.alternative_emails:
                    for email in conversation.regulator.alternative_emails:
                        choices.append((email, email))

        self.fields["to"] = forms.CharField(widget=forms.Select(choices=choices))

    class Meta:
        fields = ["to", "send_mail"]
        model = Message


class HtmlWidget(Widget):
    """A widget to display HTML in admin fields."""

    input_type = None  # Subclasses must define this.

    def render(self, name, value, attrs=None, renderer=None):
        if value is None:
            value = ""
        return mark_safe(f"<div>{value}</div>")


@register(Message)
class MessageAdmin(admin.ModelAdmin):
    form = MessageForm
    inlines = [AttachmentInline]
    list_filter = ["message_type", "message_direction", "conversation"]
    list_display = [
        "created_at",
        "subject",
        "message_type",
        "message_direction",
        "conversation",
    ]
    search_fields = ["text", "conversation__case__name"]
    readonly_fields = ["created_by", "created_at", "updated_at"]
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "subject",
                    "text",
                    "to",
                    "send_mail",
                    "message_type",
                    "message_direction",
                    "created_by",
                    "created_at",
                    "updated_at",
                    "conversation",
                ]
            },
        ),
    ]

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = super().get_readonly_fields(request, obj)
        if obj:
            return read_only_fields + [
                "subject",
                # "text",
                "to",
                "message_type",
                "message_direction",
                "conversation",
            ]
        return read_only_fields

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            form.base_fields["text"].widget = HtmlWidget()

        tpl_id = request.GET.get("tpl_id")
        if tpl_id:
            template = EmailTemplate.objects.get(pk=tpl_id)
            form.base_fields["subject"].initial = template.subject
            form.base_fields["text"].initial = template.body
        conversation_id = request.GET.get("conversation_id")
        if conversation_id:
            conversation = Conversation.objects.get(pk=conversation_id)
            form.base_fields["conversation"].initial = conversation
        return form

    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        if (
            form.instance.message_type == MessageType.EMAIL
            and form.instance.message_direction == MessageDirection.OUTBOUND
            and form.cleaned_data.get("send_mail")
        ):
            attachments = Attachment.objects.filter(message=form.instance)
            send_email_with_attachment(
                form.instance.subject, form.instance.text, form.instance.to, attachments
            )


@register(Attachment)
class AttachmentAdmin(admin.ModelAdmin):
    search_fields = ["name", "message__text"]
    readonly_fields = ["created_by", "created_at", "updated_at"]
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "name",
                    "file",
                    "created_by",
                    "created_at",
                    "updated_at",
                    "message",
                ]
            },
        ),
    ]


@register(EmailTemplate)
class EmailTemplateAdmin(admin.ModelAdmin):
    search_fields = ["name"]
    readonly_fields = ["created_by", "created_at", "updated_at"]
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "name",
                    "subject",
                    "body",
                    "created_by",
                    "created_at",
                    "updated_at",
                ]
            },
        ),
    ]
