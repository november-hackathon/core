import os

from django.conf import settings
from django.core.files import File
from django.core.mail import (
    send_mail,
    get_connection,
    EmailMultiAlternatives,
)


def send_email(subject: str, body: str, to: str):
    send_mail(
        subject,
        body,
        settings.EMAIL_FROM,
        [to],
        fail_silently=False,
    )


def send_email_with_attachment(
    subject: str, body: str, to: str, attachments: list[File]
):
    connection = get_connection(
        username=settings.EMAIL_HOST_USER,
        password=settings.EMAIL_HOST_PASSWORD,
        fail_silently=False,
    )
    mail = EmailMultiAlternatives(
        subject, body, settings.EMAIL_FROM, [to], connection=connection
    )

    for attachment in attachments:
        file_name = os.path.basename(attachment.file.name)
        file_content = attachment.file.read()
        mail.attach(file_name, file_content)

    return mail.send()
