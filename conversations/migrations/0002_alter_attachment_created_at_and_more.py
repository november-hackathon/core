# Generated by Django 4.2.7 on 2023-11-26 11:51

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import tinymce.models


class Migration(migrations.Migration):
    dependencies = [
        ("companies", "0007_alter_company_alternative_emails_and_more"),
        ("cases", "0004_alter_case_created_at_alter_case_created_by_and_more"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("conversations", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="attachment",
            name="created_at",
            field=models.DateTimeField(
                default=django.utils.timezone.now,
                editable=False,
                verbose_name="Создано",
            ),
        ),
        migrations.AlterField(
            model_name="attachment",
            name="created_by",
            field=models.ForeignKey(
                editable=False,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to=settings.AUTH_USER_MODEL,
                verbose_name="Кем создано",
            ),
        ),
        migrations.AlterField(
            model_name="attachment",
            name="file",
            field=models.FileField(upload_to="attachments/", verbose_name="Файл"),
        ),
        migrations.AlterField(
            model_name="attachment",
            name="message",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="attachments",
                to="conversations.message",
                verbose_name="Сообщение",
            ),
        ),
        migrations.AlterField(
            model_name="attachment",
            name="name",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Имя вложения"
            ),
        ),
        migrations.AlterField(
            model_name="attachment",
            name="updated_at",
            field=models.DateTimeField(
                default=django.utils.timezone.now, verbose_name="Обновлено"
            ),
        ),
        migrations.AlterField(
            model_name="conversation",
            name="case",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="conversations",
                to="cases.case",
                verbose_name="Кейс",
            ),
        ),
        migrations.AlterField(
            model_name="conversation",
            name="company",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="conversations",
                to="companies.company",
                verbose_name="Компания",
            ),
        ),
        migrations.AlterField(
            model_name="conversation",
            name="created_at",
            field=models.DateTimeField(
                default=django.utils.timezone.now,
                editable=False,
                verbose_name="Создано",
            ),
        ),
        migrations.AlterField(
            model_name="conversation",
            name="created_by",
            field=models.ForeignKey(
                editable=False,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to=settings.AUTH_USER_MODEL,
                verbose_name="Кем создано",
            ),
        ),
        migrations.AlterField(
            model_name="conversation",
            name="regulator",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="conversations",
                to="companies.regulator",
                verbose_name="Регулятор",
            ),
        ),
        migrations.AlterField(
            model_name="conversation",
            name="summary",
            field=models.TextField(
                blank=True, null=True, verbose_name="Краткое описание"
            ),
        ),
        migrations.AlterField(
            model_name="conversation",
            name="updated_at",
            field=models.DateTimeField(
                default=django.utils.timezone.now, verbose_name="Обновлено"
            ),
        ),
        migrations.AlterField(
            model_name="emailtemplate",
            name="body",
            field=tinymce.models.HTMLField(verbose_name="Текст сообщения"),
        ),
        migrations.AlterField(
            model_name="emailtemplate",
            name="created_at",
            field=models.DateTimeField(
                default=django.utils.timezone.now,
                editable=False,
                verbose_name="Создано",
            ),
        ),
        migrations.AlterField(
            model_name="emailtemplate",
            name="created_by",
            field=models.ForeignKey(
                editable=False,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to=settings.AUTH_USER_MODEL,
                verbose_name="Кем создано",
            ),
        ),
        migrations.AlterField(
            model_name="emailtemplate",
            name="name",
            field=models.CharField(max_length=255, verbose_name="Название шаблона"),
        ),
        migrations.AlterField(
            model_name="emailtemplate",
            name="subject",
            field=models.CharField(max_length=255, verbose_name="Тема"),
        ),
        migrations.AlterField(
            model_name="emailtemplate",
            name="updated_at",
            field=models.DateTimeField(
                default=django.utils.timezone.now, verbose_name="Обновлено"
            ),
        ),
        migrations.AlterField(
            model_name="message",
            name="conversation",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="messages",
                to="conversations.conversation",
                verbose_name="Диалог",
            ),
        ),
        migrations.AlterField(
            model_name="message",
            name="created_at",
            field=models.DateTimeField(
                default=django.utils.timezone.now,
                editable=False,
                verbose_name="Создано",
            ),
        ),
        migrations.AlterField(
            model_name="message",
            name="created_by",
            field=models.ForeignKey(
                editable=False,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to=settings.AUTH_USER_MODEL,
                verbose_name="Кем создано",
            ),
        ),
        migrations.AlterField(
            model_name="message",
            name="message_direction",
            field=models.CharField(
                choices=[("INBOUND", "Inbound"), ("OUTBOUND", "Outbound")],
                default="OUTBOUND",
                max_length=50,
                verbose_name="Направление сообщения",
            ),
        ),
        migrations.AlterField(
            model_name="message",
            name="message_type",
            field=models.CharField(
                choices=[("PHONE", "Phone"), ("EMAIL", "Email"), ("OTHER", "Other")],
                default="EMAIL",
                max_length=50,
                verbose_name="Тип сообщения",
            ),
        ),
        migrations.AlterField(
            model_name="message",
            name="subject",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Тема"
            ),
        ),
        migrations.AlterField(
            model_name="message",
            name="text",
            field=tinymce.models.HTMLField(verbose_name="Текст сообщения"),
        ),
        migrations.AlterField(
            model_name="message",
            name="to",
            field=models.CharField(
                blank=True, max_length=255, null=True, verbose_name="Кому"
            ),
        ),
        migrations.AlterField(
            model_name="message",
            name="updated_at",
            field=models.DateTimeField(
                default=django.utils.timezone.now, verbose_name="Обновлено"
            ),
        ),
    ]
