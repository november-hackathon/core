from gettext import gettext as _

import nanoid
from django.db import models
from tinymce.models import HTMLField

from core.models import Dated, UUIDed, CreatedBy


# Create your models here.
class MessageType(models.TextChoices):
    PHONE = "PHONE", _("Phone")
    EMAIL = "EMAIL", _("Email")
    OTHER = "OTHER", _("Other")


class MessageDirection(models.TextChoices):
    INBOUND = "INBOUND", _("Inbound")
    OUTBOUND = "OUTBOUND", _("Outbound")


class Conversation(Dated, UUIDed, CreatedBy):
    summary = models.TextField(_("Краткое описание"), null=True, blank=True)

    case = models.ForeignKey(
        verbose_name=_("Кейс"),
        to="cases.Case",
        on_delete=models.CASCADE,
        related_name="conversations",
    )
    company = models.ForeignKey(
        verbose_name=_("Компания"),
        to="companies.Company",
        on_delete=models.CASCADE,
        related_name="conversations",
        null=True,
        blank=True,
    )
    regulator = models.ForeignKey(
        verbose_name=_("Регулятор"),
        to="companies.Regulator",
        on_delete=models.CASCADE,
        related_name="conversations",
        null=True,
        blank=True,
    )

    def __str__(self):
        return f"{self.case.name}({self.created_at})"


class Message(Dated, UUIDed, CreatedBy):
    class Meta:
        ordering = ["-created_at"]

    subject = models.CharField(_("Тема"), max_length=255, null=True, blank=True)
    to = models.CharField(_("Кому"), max_length=255, null=True, blank=True)
    text = HTMLField(_("Текст сообщения"))
    message_type = models.CharField(
        verbose_name=_("Тип сообщения"),
        max_length=50,
        choices=MessageType.choices,
        default=MessageType.EMAIL,
    )
    message_direction = models.CharField(
        verbose_name=_("Направление сообщения"),
        max_length=50,
        choices=MessageDirection.choices,
        default=MessageDirection.OUTBOUND,
    )

    conversation = models.ForeignKey(
        verbose_name=_("Диалог"),
        to="Conversation",
        on_delete=models.CASCADE,
        related_name="messages",
    )

    def __str__(self):
        return self.text[:20]


class Attachment(Dated, UUIDed, CreatedBy):
    name = models.CharField(_("Имя вложения"), max_length=255, null=True, blank=True)
    file = models.FileField(_("Файл"), upload_to="attachments/")

    message = models.ForeignKey(
        verbose_name=_("Сообщение"),
        to="Message",
        on_delete=models.CASCADE,
        related_name="attachments",
    )

    def __str__(self):
        return self.name if self.name else self.file.name


class EmailTemplate(Dated, UUIDed, CreatedBy):
    class Meta:
        verbose_name_plural = "email templates"

    name = models.CharField(_("Название шаблона"), max_length=255)
    subject = models.CharField(_("Тема"), max_length=255)
    body = HTMLField(_("Текст сообщения"))

    def __str__(self):
        return self.name
