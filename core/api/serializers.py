from rest_framework import serializers


class StatusSerializer(serializers.Serializer):
    status = serializers.SerializerMethodField()

    @staticmethod
    def get_status(obj):
        return obj.get_status_display()
