from openai import OpenAI
import codex.settings as settings


class ChatGPTClient:
    def __init__(self, *args, **kwargs):
        self.client = OpenAI(api_key=settings.OPENAI_API_KEY)

    def _predict(self, *, context: str, text: str):
        response = self.client.chat.completions.create(
            model=settings.CHAT_GPT_MODEL,
            messages=[
                {"role": "system", "content": context},
                {"role": "user", "content": text},
            ]
        )
        return response.choices[0].message.content

    def get_title(self, *, text: str) -> str:
        return self._predict(context=settings.TITLE_CONTEXT, text=text)

    def get_summary(self, *, text: str) -> str:
        return self._predict(context=settings.SUMMARY_CONTEXT, text=text)
