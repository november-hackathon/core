from typing import Protocol


class AiHintsClient(Protocol):
    """Interface for AI Hints Service"""
    def get_title(self, *, text: str) -> str:
        ...

    def get_summary(self, *, text: str) -> str:
        ...

