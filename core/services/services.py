from .interfaces import AiHintsClient


class AiHintsService:
    def __init__(self, *, text: str, client: AiHintsClient):
        self.client = client
        self.text = text

    def get_title(self) -> str:
        """Get title for ticket"""
        return self.client.get_title(text=self.text)

    def get_summary(self) -> str:
        """Get title for ticket"""
        return self.client.get_summary(text=self.text)
