from uuid import UUID

from celery import shared_task
from core.services import AiHintsService
from core.clients import ChatGPTClient
from tickets.models import Ticket


@shared_task()
def fill_ticket_summary(ticket_id: UUID):
    ticket = Ticket.objects.get(pk=ticket_id)
    ai_service = AiHintsService(text=ticket.description, client=ChatGPTClient())
    ticket.title = ai_service.get_title()
    ticket.summary = ai_service.get_summary()
    ticket.save()
