from django.contrib import admin

from .models import File


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    list_display = ("pk", "created_at", "updated_at", "file")
    search_fields = ("file__name",)
    ordering = ("-created_at",)
    readonly_fields = ("created_at", "updated_at")
