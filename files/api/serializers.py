from rest_framework import serializers

from files.models import File


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        read_only_fields = ("pk",)
        fields = ("file",) + read_only_fields
