from django.urls import path, include
from rest_framework import routers

from files.api.viewsets import FileAPIView

router = routers.DefaultRouter()
router.register("files", FileAPIView)
