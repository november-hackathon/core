from drf_yasg.openapi import Response
from rest_framework import status, viewsets
from rest_framework.parsers import MultiPartParser, FormParser

from files.api.serializers import FileSerializer
from files.models import File


class FileAPIView(viewsets.ModelViewSet):
    queryset = File.objects.all().order_by("-created_at")
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = FileSerializer
