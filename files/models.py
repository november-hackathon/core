from django.db import models
from core.models import UUIDed, Dated

from django.utils.translation import gettext_lazy as _


class File(UUIDed, Dated):
    file = models.FileField(_("Файл"), upload_to="files/")

    def __str__(self):
        return self.file.name
