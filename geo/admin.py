from django.contrib import admin

from core.admin import SaveCreatedBy
from geo.models import Country


@admin.register(Country)
class CategoryAdmin(SaveCreatedBy, admin.ModelAdmin):
    list_display = ["name"]
    search_fields = ["name"]
    ordering = ["name"]
    readonly_fields = ["created_by", "created_at", "updated_at"]
