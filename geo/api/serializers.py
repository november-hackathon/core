from rest_framework import serializers

from geo.models import Country


class CountrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country
        fields = ["uid", "name"]
