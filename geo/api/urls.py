from rest_framework import routers

from geo.api.viewsets import CountryViewSet

router = routers.DefaultRouter()
router.register("countries", CountryViewSet)
