from rest_framework import viewsets, permissions, mixins

from geo.api.serializers import CountrySerializer
from geo.models import Country


class CountryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Country.objects.all().order_by("-name")
    serializer_class = CountrySerializer
    permission_classes = [permissions.AllowAny]
    pagination_class = None
