from django.db import models

from core.models import UUIDed, Dated, CreatedBy

from django.utils.translation import gettext_lazy as _


class Country(Dated, UUIDed, CreatedBy):
    class Meta:
        verbose_name_plural = "countries"

    name = models.CharField(_("Страна"), max_length=255, unique=True)

    def __str__(self):
        return self.name
