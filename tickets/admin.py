from django.contrib import admin

from core.admin import SaveCreatedBy
from tickets.models import Ticket, Category, Status


class FileInline(admin.TabularInline):
    verbose_name = "файл"
    verbose_name_plural = "файлы"
    model = Ticket.files.through
    extra = 0

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(Ticket)
class TicketAdmin(SaveCreatedBy, admin.ModelAdmin):
    inlines = [FileInline]
    list_display = [
        "created_at",
        "title",
        "category",
        "status",
        "country",
        "company",
        "created_by",
    ]
    list_filter = ["status", "category", "country", "company"]
    search_fields = ["title", "description"]
    ordering = ["-created_at"]
    readonly_fields = ["created_by", "created_at", "updated_at"]
    autocomplete_fields = ["case", "category", "country", "company"]
    fields = [
        "description",
        "requester_name",
        "title",
        "summary",
        "case",
        "category",
        "status",
        "country",
        "company",
        "company_name",
        "telegram",
        "phone",
        "email",
        "other_contact",
        "created_by",
        "created_at",
        "updated_at",
    ]

    def save_model(self, request, obj, form, change):
        if obj.case and obj.case.status != obj.status:
            obj.status = obj.case.status
        elif not obj.case:
            obj.status = Status.NEW
        return super().save_model(request, obj, form, change)


@admin.register(Category)
class CategoryAdmin(SaveCreatedBy, admin.ModelAdmin):
    list_display = ["name"]
    search_fields = ["name"]
    ordering = ["name"]
    readonly_fields = ["created_by", "created_at", "updated_at"]
