from rest_framework import serializers

from companies.api.serializers import CompanySerializer
from companies.models import Company
from core.api.serializers import StatusSerializer
from geo.api.serializers import CountrySerializer
from geo.models import Country
from tickets.models import Ticket, Status, Category


class CreateTicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = [
            "uid",
            "requester_name",
            "description",
            "category_id",
            "country_id",
            "company_id",
            "company_name",
            "telegram",
            "phone",
            "email",
            "other_contact",
            "files",
        ]

    company_id = serializers.PrimaryKeyRelatedField(
        queryset=Company.objects.all(),
        source="company",
        write_only=True,
        required=False,
        allow_null=True,
    )

    country_id = serializers.PrimaryKeyRelatedField(
        queryset=Country.objects.all(),
        source="country",
        write_only=True,
        required=True,
    )

    category_id = serializers.PrimaryKeyRelatedField(
        queryset=Category.objects.all(),
        source="category",
        write_only=True,
        required=True,
    )

    def validate(self, attrs):
        if not attrs.get("company") and not attrs.get("company_name"):
            raise serializers.ValidationError(
                "You must provide either a company or a company name"
            )
        if attrs.get("company") and attrs.get("company_name"):
            raise serializers.ValidationError(
                "You must provide either a company or a company name, not both"
            )

        if (
            not attrs.get("telegram")
            and not attrs.get("phone")
            and not attrs.get("email")
            and not attrs.get("other_contact")
        ):
            raise serializers.ValidationError(
                "You must provide at least one contact method"
            )
        if attrs.get("company") == "":
            attrs.pop("company", None)
        return attrs

    def create(self, validated_data):
        validated_data["status"] = Status.NEW
        return super().create(validated_data)


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ["uid", "name", "description"]


class TicketSerializer(StatusSerializer, serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = [
            "uid",
            "case_id",
            "title",
            "description",
            "summary",
            "status",
            "created_at",
            "country",
            "company",
            "category",
        ]

    country = CountrySerializer()
    company = CompanySerializer()
    category = CategorySerializer()
