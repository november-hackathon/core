from rest_framework import routers

from tickets.api.viewsets import TicketViewSet, CategoryViewSet

router = routers.DefaultRouter()
router.register("tickets", TicketViewSet)
router.register("categories", CategoryViewSet)
