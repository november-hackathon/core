from rest_framework import viewsets, permissions, mixins

from tickets.api.serializers import (
    TicketSerializer,
    CategorySerializer,
    CreateTicketSerializer,
)
from tickets.models import Ticket, Category
from core.tasks import fill_ticket_summary


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Category.objects.all().order_by("-created_at")
    serializer_class = CategorySerializer
    permission_classes = [permissions.AllowAny]


class TicketViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    queryset = Ticket.objects.all().order_by("-created_at")
    permission_classes = [permissions.AllowAny]

    def get_serializer_class(self):
        if self.action == "create":
            return CreateTicketSerializer
        return TicketSerializer

    def perform_create(self, serializer):
        serializer.validated_data["title"] = serializer.validated_data["description"][:20]
        instance = serializer.save()
        fill_ticket_summary.delay(instance.uid)
