from gettext import gettext as _

from django.db import models
from django_fsm import FSMField

from core.models import Dated, UUIDed, CreatedBy
from files.models import File


class Status(models.TextChoices):
    NEW = "NEW", _("New")
    IN_PROGRESS = "IN_PROGRESS", _("In Progress")
    DONE = "DONE", _("Done")


class Ticket(UUIDed, Dated, CreatedBy):
    title = models.CharField(
        _("Заголовок от AI"), max_length=255, null=True, blank=True
    )
    requester_name = models.CharField(
        _("Имя заявителя"), max_length=255, null=True, blank=True
    )
    summary = models.TextField(_("Саммари от AI"), null=True, blank=True)
    description = models.TextField(
        _("Описание проблемы от пользователя"),
    )
    issue_date = models.DateField(_("Дата инцидента"), null=True, blank=True)
    status = FSMField(_("Статус"), default=Status.NEW, choices=Status.choices)

    # contacts
    telegram = models.CharField(_("Телеграм"), max_length=255, null=True, blank=True)
    phone = models.CharField(_("Телефон"), max_length=255, null=True, blank=True)
    email = models.EmailField(_("Почта"), null=True, blank=True)
    other_contact = models.CharField(
        _("Другой способ связи"), max_length=255, null=True, blank=True
    )

    files = models.ManyToManyField(
        "files.File", verbose_name=_("файлы"), related_name="tickets", blank=True
    )
    case = models.ForeignKey(
        "cases.Case",
        verbose_name=_("Кейс"),
        on_delete=models.CASCADE,
        related_name="tickets",
        null=True,
        blank=True,
    )
    category = models.ForeignKey(
        "Category",
        verbose_name=_("Категория"),
        on_delete=models.CASCADE,
        related_name="tickets",
        null=True,
    )
    country = models.ForeignKey(
        "geo.Country",
        verbose_name=_("Страна"),
        on_delete=models.CASCADE,
        related_name="tickets",
        null=True,
    )
    company = models.ForeignKey(
        "companies.Company",
        verbose_name=_("Компания"),
        on_delete=models.CASCADE,
        related_name="tickets",
        null=True,
    )
    company_name = models.CharField(
        verbose_name=_("Название компании от пользователя"),
        max_length=255,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.title if self.title else self.description[:20]


class Category(UUIDed, Dated, CreatedBy):
    class Meta:
        verbose_name_plural = "categories"

    name = models.CharField(_("Название категории"), max_length=255)
    description = models.TextField(_("Описание"), null=True, blank=True)

    def __str__(self):
        return self.name
