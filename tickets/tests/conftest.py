import pytest
from rest_framework.reverse import reverse
from rest_framework.test import APIClient


@pytest.fixture
def admin_user(db, django_user_model):
    return django_user_model.objects.create_superuser(
        email="admin@admin.at", password="secret"
    )


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def admin_client(api_client: APIClient, admin_user):
    api_client.force_authenticate(user=admin_user)
    return api_client


@pytest.fixture
def ticket_list_url():
    return reverse("ticket-list")


@pytest.fixture
def ticket_url():
    def create_url(ticket):
        return reverse("ticket-detail", kwargs={"pk": ticket.pk})

    return create_url
