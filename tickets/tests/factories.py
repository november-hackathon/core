import factory
from factory.django import DjangoModelFactory
from pytest_factoryboy import register

from tickets.models import Ticket


class TicketFactory(DjangoModelFactory):
    class Meta:
        model = Ticket

    title = factory.Faker("sentence", nb_words=4)
    description = factory.Faker("text")
    created_by = factory.SubFactory("users.tests.factories.UserFactory")


register(TicketFactory)
