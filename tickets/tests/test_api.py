import pytest
from rest_framework import status

from tickets.models import Ticket, Status
from tickets.tests.factories import TicketFactory


@pytest.mark.django_db
def test_get_tickets_list(admin_client, ticket_list_url):
    ticket = TicketFactory.create_batch(size=5)

    resp = admin_client.get(ticket_list_url)

    assert resp.status_code == status.HTTP_200_OK
    assert resp.data["count"] == 5


@pytest.mark.django_db
def test_get_ticket(admin_client, ticket_url):
    ticket = TicketFactory.create()

    resp = admin_client.get(ticket_url(ticket))
    received_ticket = resp.data

    assert resp.status_code == status.HTTP_200_OK
    assert ticket.title == received_ticket["title"]
    assert ticket.description == received_ticket["description"]
    assert ticket.status == received_ticket["status"]
    assert str(ticket.uid) == received_ticket["uid"]


@pytest.mark.django_db
def test_create_ticket(admin_client, ticket_list_url):
    ticket = TicketFactory()
    data = {
        "title": ticket.title,
        "description": ticket.description,
    }
    resp = admin_client.post(ticket_list_url, data=data)
    result = resp.data

    assert resp.status_code == status.HTTP_201_CREATED
    assert ticket.title == result["title"]
    assert ticket.description == result["description"]
    assert ticket.status == Status.NEW


@pytest.mark.django_db
def test_update_ticket(admin_client, ticket_url):
    ticket = TicketFactory.create()

    data = {
        "title": "New title",
        "description": "New description",
        "status": Status.IN_PROGRESS,
    }

    resp = admin_client.patch(ticket_url(ticket), data=data)

    assert resp.status_code == status.HTTP_200_OK
    assert data["title"] == resp.data["title"]
    assert data["description"] == resp.data["description"]
    assert data["status"] == resp.data["status"]
    assert str(ticket.uid) == resp.data["uid"]


@pytest.mark.django_db
def test_delete_ticket(admin_client, ticket_url):
    ticket = TicketFactory.create()

    resp = admin_client.delete(ticket_url(ticket))

    assert resp.status_code == status.HTTP_204_NO_CONTENT
    assert resp.data is None
    assert Ticket.objects.count() == 0
