from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from gettext import gettext as _
from users.models import CustomUser


@admin.register(CustomUser)
class UserAdmin(UserAdmin):
    fieldsets = [
        (None, {"fields": ["email", "password"]}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
    ]
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "password1",
                    "password2",
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
    )
    list_display = ["email", "is_staff"]

    search_fields = ["email"]
    ordering = ["email"]
