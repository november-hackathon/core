from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from core.models import UUIDed, Dated
from .managers import CustomUserManager


class CustomUser(Dated, UUIDed, AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    email = models.EmailField(_("Адрес email"), unique=True, db_index=True)
    is_staff = models.BooleanField(_("Сотрудник"), default=False)
    is_active = models.BooleanField(_("Активен"), default=True)
    date_joined = models.DateTimeField(_("Дата регистрации"), default=timezone.now)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email
