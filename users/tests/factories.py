import factory
from factory.django import DjangoModelFactory
from pytest_factoryboy import register

from users.models import CustomUser


class UserFactory(DjangoModelFactory):
    class Meta:
        model = CustomUser
        django_get_or_create = ("email",)

    email = factory.Faker("email")


register(UserFactory)
